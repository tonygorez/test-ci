const { test } = require("./index");

describe("Test func", () => {
    it("should be foo to the bar", () => {
        expect(test()).toStrictEqual({ foo: "bar" })
    });

    it("should return a custom foo property", () => {
        const customFooProperty = "baz";
        const expectedResult = { foo: customFooProperty };

        expect(test(customFooProperty)).toStrictEqual(expectedResult);
    })
});