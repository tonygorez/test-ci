
function test(customForProperty) {
    if (customForProperty !== null &&
        customForProperty !== undefined &&
        typeof customForProperty === "string") {
        return { foo: customForProperty };
    }

    return { foo: "bar" };
}

module.exports = {
    test
}